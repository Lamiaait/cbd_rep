package com.cbd.integrationtests;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.net.URL;
import javax.ws.rs.core.Response;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import com.cbd.commonutils.ResourceClient;
import com.cbd.commonutils.ResourceDefinitions;

import com.cbd.commonmodel.HttpCode;
import static com.cbd.commonutils.FileTestNameUtils.*;

@RunWith(Arquillian.class)
public class EmployeeIntTest {
	@ArquillianResource
	private URL url; 

	private ResourceClient resourceClient;

	private static final String PATH_RESOURCE = ResourceDefinitions.Employee.getResourceName();

	@Deployment
	public static WebArchive createDeployment() {
		return ShrinkWrap
				.create(WebArchive.class)
				.addPackages(true, "com.cbd")
				.addAsResource("import.sql", "META-INF/import.sql")
				.addAsResource("persistence-integration.xml", "META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
				.setWebXML(new File("src/test/resources/web.xml"))
				.addAsLibraries(
						Maven.resolver().resolve("com.google.code.gson:gson:2.3.1", 
								"org.mockito:mockito-core:1.9.5", 
								"org.hibernate:hibernate-core:5.3.7.Final")
						.withTransitivity().asFile());
	}

	@Before
	public void initTestCase() {
		this.resourceClient = new ResourceClient(url);
		resourceClient.resourcePath("/DB").delete();
		System.out.println("URL is "+url);
	}

	@Test
	@RunAsClient
	public void testGetAllProducts() {

		final Response response = resourceClient.resourcePath(PATH_RESOURCE).get();
		assertEquals(response.getStatus(), HttpCode.OK.getCode());
	}
	
	@Test
	@RunAsClient
	public void testAddEmployee() {
		
		final Response response = resourceClient.resourcePath(PATH_RESOURCE)
				.postWithFile(getPathFileRequest(PATH_RESOURCE, "employees.json"));
		
		assertEquals(HttpCode.CREATED.getCode(), response.getStatus());
	}

	@Test
	@RunAsClient
	public void testDeleteEmployee(){
		resourceClient.resourcePath(PATH_RESOURCE)
		.postWithFile(getPathFileRequest(PATH_RESOURCE, "employees.json"));
		final Response responseGet = resourceClient.resourcePath(PATH_RESOURCE + "/3").delete();
		assertEquals(HttpCode.NO_CONTENT.getCode(), responseGet.getStatus());
	}
	
	@Test
	@RunAsClient
	public void testAddEmployeeAndGetIt() {
		
		resourceClient.resourcePath(PATH_RESOURCE)
				.postWithFile(getPathFileRequest(PATH_RESOURCE, "employees.json"));
		final Response get = resourceClient.resourcePath(PATH_RESOURCE + "/3").get();
		assertEquals(get.getStatus(), HttpCode.OK.getCode());
		
	}
}
