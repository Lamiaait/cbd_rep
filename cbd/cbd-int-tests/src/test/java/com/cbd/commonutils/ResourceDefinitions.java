package com.cbd.commonutils;

import org.junit.Ignore;

@Ignore
public enum ResourceDefinitions {
	Employee("employees");
	private String resourceName;

	ResourceDefinitions(final String resourceName) {
		this.resourceName = resourceName;
	}

	public String getResourceName() {
		return resourceName;
	}
}
