package com.cbd.commonutils;

import org.junit.Ignore;

@Ignore
public class FileTestNameUtils {
	private static final String PATH_REQUEST = "/request/";
	
	private FileTestNameUtils() {	
	}
	
	public static String getPathFileRequest(final String mainFolder, final String fileName) {
		return mainFolder+PATH_REQUEST+fileName;
	}

}
