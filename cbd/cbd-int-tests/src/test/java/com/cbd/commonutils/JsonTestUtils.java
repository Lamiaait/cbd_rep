package com.cbd.commonutils;

import java.io.InputStream;
import java.util.Scanner;
import org.junit.Ignore;

@Ignore
public class JsonTestUtils {
	public static final String BASE_JSON_DIR = "json/";

	private JsonTestUtils() {
	}

	public static String readJsonFile(final String relativePath) {
		final InputStream inputStream = JsonTestUtils.class.getClassLoader().getResourceAsStream(BASE_JSON_DIR + relativePath);
		try (Scanner scanner = new Scanner(inputStream)) {
			return scanner.useDelimiter("\\A").hasNext() ? scanner.next() : "";
		}
	}
	
}