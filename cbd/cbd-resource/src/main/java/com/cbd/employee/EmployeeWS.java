package com.cbd.employee;

import java.util.List;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/employees")
@Stateless
@LocalBean
@Produces({MediaType.APPLICATION_JSON})
public class EmployeeWS {
	
	@EJB
	EmployeeDAO employeeDAO;
	
	@GET
	public Response findAll() {
		final List<Employee> employees = employeeDAO.getAllEmployees();
		return Response.status(200).entity(employees).build();
	}
	
	@GET
	@Path("/{employeeId}")
	public Response findEmployeeById(@PathParam("employeeId") final int employeeId) {
		final Employee employee =employeeDAO.getEmployee(employeeId);
		return Response.status(200).entity(employee).build();
	}	
	
	@POST
	public Response saveEmployee(final Employee employee) {
		employeeDAO.save(employee);
		return Response.status(201).entity(employee).build();
	}
	
	@PUT
	@Path("/{employeeId}")
	@Consumes({MediaType.APPLICATION_JSON})
	public Response updateEmployee(final Employee employee) {
		employeeDAO.update(employee);
		return Response.status(200).entity(employee).build();
	}
	
	@DELETE
	@Path("/{employeeId}")
	public Response deleteEmployee(@PathParam("employeeId") final int employeeId) {
		employeeDAO.delete(employeeId);
		return Response.status(204).build();
	}

}
