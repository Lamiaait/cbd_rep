package com.cbd.commonmodel;

public enum HttpCode {
	CREATED(201),
	VALIDATION_ERROR(422),
	OK(200),
	CONFLICT(409),
	NOT_FOUND(404),
	NO_CONTENT(204),
	FORBIDDEN(403);

	private int code;

	HttpCode(final int code) {
		this.code = code;
	}

	public int getCode() {
		return code;
	}

}