package com.cbd.employee;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;

import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;

import com.cbd.commonmodel.HttpCode;
import static  com.cbd.commonutils.ModelsForTestUtils.*;

public class EmployeeWSTest {
	EmployeeWS ws;
	EmployeeDAO dao;
	
	@Before
	public void setUp() throws Exception {
		ws = new EmployeeWS();
		dao = mock(EmployeeDAO.class);
		ws.employeeDAO =dao;
	}
	@Test
	public void findAllEmployeesTest() {
		when(dao.getAllEmployees()).thenReturn(allEmployees());
		Response res = ws.findAll();
		assertEquals(HttpCode.OK.getCode(), res.getStatus());
		@SuppressWarnings("unchecked")
		List<Employee> returnedList = (List<Employee>) res.getEntity();
		assertEquals("John",returnedList.get(0).getName());
	}
		
	@Test
	public void findEmployeeByIdTest() {
		when(dao.getEmployee(1)).thenReturn(john());
		Response res = ws.findEmployeeById(1);
		assertEquals(HttpCode.OK.getCode(), res.getStatus());
		Employee returnedEmployee = (Employee) res.getEntity();
		assertEquals(returnedEmployee.getName(), john().getName());
	}	
	@Test
	public void updateEmployeeTest() {
		Response res = ws.updateEmployee(john());
		assertEquals(HttpCode.OK.getCode(), res.getStatus());		
	}	
	@Test
	public void addEmployeetTest() {
		when(dao.save(john())).thenReturn(john());
		Response res =  ws.saveEmployee(john());
		assertEquals(HttpCode.CREATED.getCode(), res.getStatus());
		Employee returnedEmployee = (Employee) res.getEntity();
		assertEquals(returnedEmployee.getName(), john().getName());
	}
	
	@Test
	public void deleteEmployee() {
		Response res =  ws.deleteEmployee(1);
		assertEquals(HttpCode.NO_CONTENT.getCode(), res.getStatus());	
	}
}
