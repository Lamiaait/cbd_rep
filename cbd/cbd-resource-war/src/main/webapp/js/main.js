// The root URL for the RESTful services
var rootURL = "http://localhost:8080/cbd/api/employees";

var currentEmployee;


var newEmployee=function () {
	$('#btnDelete').hide();
	currentEmployee = {};
	renderDetails(currentEmployee); // Display empty form
};

var findAll=function() {
	console.log('findAll');
	$.ajax({type: 'GET',url: rootURL,dataType: "json", success: renderList
	});
};

var findById= function(id) {
	console.log('findById: ' + id);
	$.ajax({
		type: 'GET',
		url: rootURL + '/' + id,
		dataType: "json",
		success: function(data){
			$('#btnDelete').show();
			console.log('findById success: ' + data.name);
			currentEmployee = data;
			renderDetails(currentEmployee);
		}
	});
};

var addEmployee = function () {
	console.log('addEmployee');
	$.ajax({
		type: 'POST',
		contentType: 'application/json',
		url: rootURL,
		dataType: "json",
		data: formToJSON(),
		success: function(data, textStatus, jqXHR){
			alert('Employee added successfully');
			$('#btnDelete').show();
			$('#id').val(data.id);
                        findAll();
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('addEmployee error: ' + textStatus);
		}
	});
};

var updateEmployee= function () {
	console.log('updateEmployee');
	$.ajax({
		type: 'PUT',
		contentType: 'application/json',
		url: rootURL + '/' + $('#id').val(),
		dataType: "json",
		data: formToJSON(),
		success: function(data, textStatus, jqXHR){
			alert('Employee updated successfully');
                         findAll();
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('updateEmployee error: ' + textStatus);
		}
	});
};

var deleteEmployee=function() {
	console.log('deleteEmployee');
	$.ajax({
		type: 'DELETE',
		url: rootURL + '/' + $('#id').val(),
		success: function(data, textStatus, jqXHR){
			alert('Employee deleted successfully');
            findAll();
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('deleteEmployee error');
		}
	});
};

var renderList= function(list) {
	// JAX-RS serializes an empty list as null, and a 'collection of one' as an object (not an 'array of one')
	//var list = data == null ? [] : (data instanceof Array ? data : [data]);
	$('#employeeList').html("");
	$.each(list, function(index, employee) {
		$('#employeeList').append('<tr><th><a href="#" id="' + employee.id + '">'+employee.id + ' </th><td>'+employee.name+'</a></td></tr>');
	});
};

var renderDetails=function(employee) {
	$('#id').val(employee.id);
	$('#name').val(employee.name);
	$('#department').val(employee.department)
};

// Helper function to serialize all the form fields into a JSON string
var formToJSON=function () {
	var id = $('#id').val();
	return JSON.stringify({
		"id": id == "" ? null : id, 
		"name": $('#name').val(), 
		"department": $('#department').val(),
		});
};

//When the DOM is ready.
$(document).ready(function(){
	// Nothing to delete in initial application state
	$('#btnDelete').hide();

	$('#btnSave').click(function() {
		if ($('#id').val() == '')
			addEmployee();
		else
			updateEmployee();
		return false;
	});

	$('#btnDelete').click(function() {
		deleteEmployee();
		return false;
	});

	
	$(document).on("click", '#employeeList a', function(){findById(this.id);});

	//reset form
	$('#id').val("");
	$('#name').val("");
	$('#department').val("");
	findAll();
});

