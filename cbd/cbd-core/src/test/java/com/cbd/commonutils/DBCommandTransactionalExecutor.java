package com.cbd.commonutils;

import javax.persistence.EntityManager;

import org.junit.Ignore;

@Ignore
public class DBCommandTransactionalExecutor {
	final private EntityManager entityManager;

	public DBCommandTransactionalExecutor(final EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public <T> T executeCommand(final DBCommand<T> dbCommand) {
		try {
			entityManager.getTransaction().begin();
			final T toReturn = dbCommand.execute();
			entityManager.getTransaction().commit();
			entityManager.clear();
			return toReturn;
		} catch (final Exception e) {
			e.printStackTrace();
			entityManager.getTransaction().rollback();
			throw new IllegalStateException(e);
		}
	}

}