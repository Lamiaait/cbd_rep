package com.cbd.commonutils;

import java.util.Arrays;
import java.util.List;
import org.junit.Ignore;
import com.cbd.employee.Employee;

@Ignore
public final class ModelsForTestUtils {
	
	public static Employee john() {
		final Employee john = new Employee("John");
		john.setDepartment("acc");
		return john;		
	}

	public static List<Employee> allEmployees() {
		return Arrays.asList(john());
	}
		
}
