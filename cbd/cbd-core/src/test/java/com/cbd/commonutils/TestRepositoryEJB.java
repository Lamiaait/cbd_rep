package com.cbd.commonutils;

import java.util.Arrays;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Ignore;

import com.cbd.employee.Employee;

@Ignore
@Stateless
public class TestRepositoryEJB {

	@PersistenceContext
	private EntityManager entityManager;

	private static final List<Class<?>> ENTITIES_TO_REMOVE = Arrays.asList(Employee.class);

	public void deleteAll() {
		for (final Class<?> entityClass : ENTITIES_TO_REMOVE) {
			deleteAllForEntity(entityClass);
		}
	}

	@SuppressWarnings("unchecked")
	private void deleteAllForEntity(final Class<?> entityClass) {
		final List<Object> rows = entityManager.createQuery("Select e From " + entityClass.getSimpleName() + " e").getResultList();
		for (final Object row : rows) {
			entityManager.remove(row);
		}
	}

}