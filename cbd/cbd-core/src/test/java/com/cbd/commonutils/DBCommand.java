package com.cbd.commonutils;

import org.junit.Ignore;

@Ignore
public interface DBCommand<T> {

	T execute();

}