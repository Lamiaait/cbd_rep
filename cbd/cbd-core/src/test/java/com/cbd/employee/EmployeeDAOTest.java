package com.cbd.employee;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.text.ParseException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.transaction.SystemException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static com.cbd.commonutils.ModelsForTestUtils.*;
import com.cbd.commonutils.DBCommandTransactionalExecutor;


public class EmployeeDAOTest {
	private EntityManagerFactory emf;
	private EntityManager entityManager;
	private EmployeeDAO dao;
	private DBCommandTransactionalExecutor dBCommandTransactionalExecutor;

	@Before
	public void initTestCase() throws SystemException, ParseException {
	emf = Persistence.createEntityManagerFactory("employeePU");
	entityManager = emf.createEntityManager();

	dao = new EmployeeDAO();
	dao.setEntityManager(entityManager);
	dBCommandTransactionalExecutor = new DBCommandTransactionalExecutor(entityManager);
	
	}
	@After
	public void closeEntityManager() {
		entityManager.close();
		emf.close();
	}

	@Test
	public void addEmployeeAndGetItById() {
		final String product = dBCommandTransactionalExecutor.executeCommand(() -> {
			
				return  dao.save((john())).getName();		
			
		});
		assertEquals(john().getName(), product);
		assertEquals(dao.getEmployee(1).getName(),john().getName());
		assertEquals(dao.getEmployee(1).getDepartment(),john().getDepartment());
	}
	@Test
	public void getAllEmployees() {
		dBCommandTransactionalExecutor.executeCommand(() -> {
		allEmployees().forEach(dao::save);
		return null;
	});
		final List<Employee> list = dao.getAllEmployees();
		assertThat(list.size(), is(equalTo(1)));
		assertThat(list.get(0).getName(),  is(equalTo(john().getName())));
		assertThat(list.get(0).getDepartment(),  is(equalTo(john().getDepartment())));
	}
	
	@Test
	public void deleteAndGetNoEmployees() {
		final String employee = dBCommandTransactionalExecutor.executeCommand(() -> {			
			return  dao.save((john())).getName();		
	});
		assertEquals(john().getName(), employee);
		dBCommandTransactionalExecutor.executeCommand(() -> {
			dao.delete(1);
			return null;
		});
		final List<Employee> list = dao.getAllEmployees();
		assertThat(list.size(), is(equalTo(0)));
	}
	@Test
	public void updateEmployee() {
		final Integer employeeAddedId = dBCommandTransactionalExecutor.executeCommand(() -> {
			return dao.save(john()).getId();
		});
		final Employee employeeAfterAdd = dao.getEmployee(employeeAddedId);
		assertThat(employeeAfterAdd.getName(), is(equalTo(john().getName())));
		employeeAfterAdd.setName("new name");
		dBCommandTransactionalExecutor.executeCommand(() -> {
			dao.update(employeeAfterAdd);
			return null;
		});
		final Employee productAfterUpdate = dao.getEmployee(employeeAddedId);
		assertThat(productAfterUpdate.getName(),is(equalTo("new name")));
	}
	
}
