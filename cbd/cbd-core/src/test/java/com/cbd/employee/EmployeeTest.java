package com.cbd.employee;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class EmployeeTest {
	
	Employee employee = new Employee();
	private static final int id =1;
	private static final String name ="name";	
	private static final String nameModified ="nameModified";	
	private static final String department= "IT";
	
	@Test
	public void testGetters() {
		employee.setDepartment(department);
		assertEquals(department, employee.getDepartment());
		employee.setId(id);
		assertEquals(id, employee.getId());
		employee.setName(name);
		assertEquals(name, employee.getName());

	}
	@Test 
	public void testConstructor() {
		employee.setName(nameModified);
		assertEquals(nameModified, employee.getName());
	}
}
