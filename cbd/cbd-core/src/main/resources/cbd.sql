-- create database
DROP DATABASE IF EXISTS CbdDb;
CREATE DATABASE CbdDb;
--
--
-- Database: `CbdDb`
--
-- --------------------------------------------------------

--
-- Table structure for table `Employee`
--
USE CbdDb;
CREATE TABLE IF NOT EXISTS `Employee` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `department` varchar(255) NOT NULL,
   PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Employee`
--

INSERT INTO `Employee` (`id`, `name`, `department`) VALUES
(1, 'John Smith', 'Accounting'),
(2, 'Peter Moore', 'Accounting'),
(3, 'Enda Kennedy', 'Accounting'),
(4, 'Brad Pitt', 'Human Resources'),
(5, 'Kate Hudson', 'Human Resources'),
(6, 'Angelina Jolie', 'Human Resources'),
(7, 'Pierce Brosnan', 'Engineering'),
(8, 'Thomas Edison', 'Engineering'),
(9, 'Marie Corie', 'Engineering'),
(10, 'Pamela Anderson', 'Engineering');
