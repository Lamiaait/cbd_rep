package com.cbd.employee;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;


@Stateless
@LocalBean
public class EmployeeDAO {
	
	@PersistenceContext
	private EntityManager entityManager;

	public void setEntityManager(final EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	@SuppressWarnings("unchecked")
	public List<Employee> getAllEmployees() {
		final Query query = entityManager.createQuery("SELECT e FROM Employee e", Employee.class);
		return query.getResultList();
	}	
	
	public Employee save(final Employee employee) {
		entityManager.persist(employee);
		return employee;
	}
	
	public Employee update(Employee employee) {
		entityManager.merge(employee);
		return employee;
	}
	
	public void delete(final int employeeId) {
		entityManager.remove(getEmployee(employeeId));
	}
	
	public Employee getEmployee(final int employeeId) {
		return entityManager.find(Employee.class, employeeId);
	}

}
